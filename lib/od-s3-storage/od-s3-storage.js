import Amplify from '@aws-amplify/core';
import Auth from '@aws-amplify/auth';
import Storage from '@aws-amplify/storage';

export class ODS3Storage {  

    constructor( cognitoConfig, s3Config ) {

        //configure using static files
        Amplify.configure({
            Auth: {
        
                // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
                identityPoolId: cognitoConfig.identityPoolId,
        
                // REQUIRED - Amazon Cognito Region
                region: cognitoConfig.region,

                // OPTIONAL - Amazon Cognito User Pool ID
                userPoolId: cognitoConfig.userPoolId,
        
                // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
                userPoolWebClientId: cognitoConfig.userPoolWebClientId
            },
            Storage: {
                AWSS3: {
                    bucket: s3Config.bucket, //REQUIRED -  Amazon S3 bucket name
                    region: s3Config.region, //OPTIONAL -  Amazon service region
                }
            }
        });
    }

    async getPublic( fileKey, callback ) {
        let fileData = null;
        let err = null;
        try {
            fileData = await Storage.get( fileKey );
        } catch ( error ) {
            console.log( 'Error trying to get public object: ', error );
            err = error
        }
        return callback( fileData, err )
    }

    async getProtected( fileKey, callback ) {
        let fileData = null;
        let err = null;
        try {
            fileData = await Storage.get( fileKey, { level: 'protected' } );
        } catch ( error ) {
            console.log( 'Error trying to get protected object: ', error );
            err = error
        }
        return callback( fileData, err )
    }

    async getOtherProtected( fileKey, identityId, callback ) {
        let fileData = null;
        let err = null;
        try {
            fileData = await Storage.get( fileKey, { level: 'protected', identityId: identityId } );
        } catch ( error ) {
            console.log( 'Error trying to get other users protected object: ', error );
            err = error
        }
        return callback( fileData, err )
    }

    async getPrivate( fileKey, callback ) {
        let fileData = null;
        let err = null;
        try {
            fileData = await Storage.get( fileKey, { level: 'private' } );
        } catch ( error ) {
            console.log( 'Error trying to get private object: ', error );
            err = error
        }
        return callback( fileData, err )
    }

    async listAllPublic( pathPrefix, callback ) {
        let results = null;
        let err = null;
        try {
            results = await Storage.list( pathPrefix );
            results = this._processStorageList( results );
        } catch ( error ) {
            console.log( 'Error trying to retrieve public objects: ', error );
            err = error
        }
        return callback( results, err )
    }

    async listAllProtected( pathPrefix, callback ) {
        let results = null;
        let err = null;
        try {
            results = await Storage.list( pathPrefix, { level: 'protected' } );
            results = this._processStorageList( results );
        } catch ( error ) {
            console.log( 'Error trying to retrieve protected objects: ', error );
            err = error
        }
        return callback( results, err )
    }

    async listOtherProtected( pathPrefix, identityId, callback ) {
        let results = null;
        let err = null;
        try {
            results = await Storage.list( pathPrefix, { level: 'protected', identityId: identityId } );
            results = this._processStorageList( results );
        } catch ( error ) {
            console.log( 'Error trying to retrieve other users protected objects: ', error );
            err = error
        }
        return callback( results, err )
    }

    async listAllPrivate( pathPrefix, callback ) {
        let results = null;
        let err = null;
        try {
            results = await Storage.list( pathPrefix, { level: 'private' } );
            results = this._processStorageList( results );
        } catch ( error ) {
            console.log( 'Error trying to retrieve private objects: ', error );
            err = error
        }
        return callback( results, err )
    }

    //from aws docs
    _processStorageList( results ) {
        const filesystem = {}
        const add = (source, target, item) => {
            const elements = source.split("/");
            const element = elements.shift();
            if( !element ) return // blank
            target[element] = target[element] || {__data: item}// element;
            if( elements.length ) {
                target[element] = typeof target[element] === "object" ? target[element] : {};
                add( elements.join("/"), target[element], item );
            }
        };
        results.forEach( item => add( item.key, filesystem, item ) );
        return filesystem
    }

    async uploadPublic( pathName, file, callback ) {
        let outputKey = null;
        let err = null;
        try {
            outputKey = await Storage.put( pathName, file );
        } catch ( error ) {
            console.log( 'Error trying to upload public object: ', error );
            err = error
        }
        return callback( outputKey, err )
    }

    async uploadProtected( pathName, file, callback ) {
        let outputKey = null;
        let err = null;
        try {
            outputKey = await Storage.put( pathName, file, { level: 'protected' } );
        } catch ( error ) {
            console.log( 'Error trying to upload protected object: ', error );
            err = error
        }
        return callback( outputKey, err )
    }    

    async uploadPrivate( pathName, file, callback ) {
        let outputKey = null;
        let err = null;
        try {
            outputKey = await Storage.put( pathName, file, { level: 'private' } );
        } catch ( error ) {
            console.log( 'Error trying to upload private object: ', error );
            err = error
        }
        return callback( outputKey, err )
    }

    async removePublic( fileKey, callback ) {
        let fileData = null;
        let err = null;
        try {
            fileData = await Storage.remove( fileKey );
        } catch ( error ) {
            console.log( 'Error trying to remove public object: ', error );
            err = error
        }
        return callback( fileData, err )
    }

    async removeProtected( fileKey, callback ) {
        let fileData = null;
        let err = null;
        try {
            fileData = await Storage.remove( fileKey, { level: 'protected' } );
        } catch ( error ) {
            console.log( 'Error trying to remove protected object: ', error );
            err = error
        }
        return callback( fileData, err )
    }

    async removePrivate( fileKey, callback ) {
        let fileData = null;
        let err = null;
        try {
            fileData = await Storage.remove( fileKey, { level: 'private' } );
        } catch ( error ) {
            console.log( 'Error trying to remove private object: ', error );
            err = error
        }
        return callback( fileData, err )
    }
} 
